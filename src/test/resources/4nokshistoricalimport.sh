#!/bin/sh
exec scala -classpath /i/p/lightsource/gitrepos/tscommon/build/libs/tscommon-all-0.1-SNAPSHOT.jar "$0" "$@"
!#

		import java.io.File
		import java.util.Date
		import scala.io.Source
		import scala.runtime.ZippedTraversable3.zippedTraversable3ToTraversable
		import org.joda.time.DateTime
		import org.joda.time.DateTimeZone
		import org.joda.time.format.DateTimeFormat
		import com.lightsource.monitoring.common.MongoStorage
		import com.lightsource.monitoring.common.TimeSeries
		import com.mongodb.BasicDBObject
		import com.mongodb.casbah.commons.MongoDBObject


	  def fileResource(resourcePath: String) = new File(resourcePath).toURI()
      val f = fileResource(args(0))
      
      val bufferedSrc = Source.fromFile(f)
      val srcLines = bufferedSrc.getLines().toList

      val res4 = srcLines map (l => l.split(";"))

      println(res4.head.toList)

      val olabels = res4.head.toList

      val dblabels = List("generpwr", "generener", "genenerdelta", "soldpwr", "soldener", "bougpwr", "bougenertot", "bougenerf1", "bougenerf2", "conspwr", "consener", "consenerdelta", "extalarm1", "extalarm2", "eliosflag", "bougenerf3")

      val regex = """\[(.*?)\]"""r

      val units = olabels.tail.map { x => regex.findFirstMatchIn(x).map(rm => rm.group(1)) }

      println(units)

      olabels.tail.zip(dblabels) map (t => println(t._1 + " " + t._2))

      val makeDouble = (s: String) => {
        try {
          s.replace(',', '.').toDouble
        } catch {
          case nfe: NumberFormatException => -1
        }
      }

      val res5 = res4.tail.map(t => {

        val firstcol = t.head
        println(firstcol)
        val regex3 = """"=""(.*?)"""""r

        val dtstring = regex3.findFirstMatchIn(firstcol).map { rm => rm.group(1) }.get

        val ldt = DateTimeFormat.forPattern("dd-MM-yy HH:mm:ss").parseLocalDateTime(dtstring)
        println(ldt)
        println(ldt.toLocalDate())
//        val ldt2 = new LocalDateTime(ldt.getYear, ldt.getMonthOfYear, ldt.getDayOfMonth, ldt.getHourOfDay, ldt.getMinuteOfHour, ldt.getSecondOfMinute)
//        println(ldt2.toDateTime(DateTimeZone.UTC))
        println(ldt.toDateTime(DateTimeZone.UTC))
        
        val dtZ = ldt.toDateTime(DateTimeZone.UTC)

        val data = t.tail

        val regex2 = """"(.*?)""""r

        val unquoteddata = data.toList.flatMap { quotedstr => regex2.findFirstMatchIn(quotedstr).map { rm => rm.group(1) } }

        println(data.toList)

        (unquoteddata, dblabels, units).zipped.toList.map { v =>
          TimeSeries(v._2,
            args(1) + "-" + v._2,
            ldt.toLocalDate(),
            List(dtZ),
            List(makeDouble(v._1)),
            None,
            v._3,
            List(makeDouble(v._1)),
            false)
        }

      })

      val res6 = res5.flatten

      res6 foreach println
      
      res6 foreach MongoStorage.storeTimeseries