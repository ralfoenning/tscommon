#!/bin/sh
exec scala -classpath /i/p/lightsource/gitrepos/tscommon/build/libs/tscommon-all-0.1-SNAPSHOT.jar "$0" "$@"
!#

		import java.io.File
		import java.util.Date
		import scala.io.Source
		import scala.runtime.ZippedTraversable3.zippedTraversable3ToTraversable
		import org.joda.time.DateTime
		import org.joda.time.DateTimeZone
		import org.joda.time.format.DateTimeFormat
		import com.lightsource.monitoring.common.MongoStorage
		import com.lightsource.monitoring.common.TimeSeries
		import com.mongodb.BasicDBObject
		import com.mongodb.casbah.commons.MongoDBObject
        import com.mongodb.casbah.Imports._
        
        if (args(1)=="cumulative") {

	      def days(dt: DateTime): Stream[DateTime] = dt #:: days(dt.minusDays(1))
	      
	      val daysstartofyear = days(DateTime.now()) take (DateTime.now().getDayOfYear)
	      val lastondaydbos = daysstartofyear.flatMap { dt => MongoStorage.findOneReading(("timestamp" $lte dt ) ++ ("meterId" $eq args(0)),  MongoDBObject(), ("timestamp" $eq -1)) }
	      val lastondayts = lastondaydbos map MongoStorage.asTimeseries
	      lastondayts foreach MongoStorage.storeDayTotal2
      
	    }
