package com.lightsource

import com.github.fakemongo.Fongo
import com.lightsource.monitoring.mongo.FongoStorage
import com.lightsource.monitoring.alerting.Alerting
package object testinjections {
  val fakedb = new Fongo("fakemongo").getDB("meterdb")

  def produceAlarmActions = Alerting(FongoStorage).produceAlarmActions

}