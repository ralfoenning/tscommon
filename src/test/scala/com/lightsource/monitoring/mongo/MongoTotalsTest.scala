package com.lightsource.monitoring.mongo

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.DuplicateKeyException
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import com.lightsource.monitoring.common.{ TimeSeries, MongoStorage }
import org.json4s.native.JsonParser
import org.json4s.native.JsonMethods._
import org.json4s._
import org.json4s.JsonDSL._
import com.github.fakemongo.Fongo
import org.joda.time.DateTimeZone

@RunWith(classOf[JUnitRunner])
class MongoTotalsTest extends Specification {
  
  sequential

  "Totals" should {

    "always be produced with timestamp from midnight" in {

      val dt = DateTime.parse("2015-03-29T12:30:00.000Z").toDateTime(DateTimeZone.UTC)
      val ld = dt.toLocalDate()

      FongoStorage.meterdb.getCollection("readings").remove(new MongoDBObject())
      FongoStorage.meterdb.getCollection("totals").drop()

      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt, dt.plusMinutes(12)), List(1.8, 1.9)))

      import scala.collection.JavaConversions._

      val all = MongoDBObject()
      FongoStorage.meterdb.getCollection("totals").find(all, MongoDBObject("_id" -> 0) ++ ("timestamp" -> 1)).toArray().toList.head.toString() must_== """{ "timestamp" : { "$date" : "2015-03-29T00:00:00.000Z"}}"""
      FongoStorage.meterdb.getCollection("totals").find(all, MongoDBObject("_id" -> 0) ++ ("value" -> 1)).toArray().toList.head.toString() must_== """{ "value" : 3.7}"""
      FongoStorage.meterdb.getCollection("totals").find(all, MongoDBObject("_id" -> 1)).toArray().toList.head.toString() must_== """{ "_id" : "id2-2015-03-29T00:00:00.000Z-dayTotal-gen"}"""

      ok
    }
    "not be produced for single datetime/value timeseries" in {

      val dt = DateTime.parse("2015-03-29T12:30:00.000Z").toDateTime(DateTimeZone.UTC)
      val ld = dt.toLocalDate()

      FongoStorage.meterdb.getCollection("readings").remove(new MongoDBObject())
      FongoStorage.meterdb.getCollection("totals").drop()

      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt), List(1.8)))

      import scala.collection.JavaConversions._

      FongoStorage.meterdb.getCollection("totals").count() must_==0
    }

  }

}