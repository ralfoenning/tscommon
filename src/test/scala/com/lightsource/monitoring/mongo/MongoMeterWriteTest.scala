package com.lightsource.monitoring.mongo

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.mongodb.DuplicateKeyException
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import com.lightsource.monitoring.common.{TimeSeries, MongoStorage}
import org.json4s.native.JsonParser
import org.json4s.native.JsonMethods._
import org.json4s._
import org.json4s.JsonDSL._
import com.github.fakemongo.Fongo
import org.joda.time.DateTimeZone

@RunWith(classOf[JUnitRunner])
class MongoMeterWriteTest extends Specification {

  sequential

  "write meter value" should {

    "work directly" in {

      val anycoll = new Fongo("fake").getDB("any").getCollection("any")

      anycoll.remove(MongoDBObject("_id" -> "testobj1234"))
      anycoll.insert(MongoDBObject("_id" -> "testobj1234") ++ ("anotherval" -> 13))

      anycoll.insert(MongoDBObject("_id" -> "testobj1234") ++ ("anotherval" -> 12)) must throwA[DuplicateKeyException]

      anycoll.find().next().get("anotherval") must_== 13
      anycoll.remove(MongoDBObject("_id" -> "testobj1234"))

      ok
    }

    "work directly II" in {

      val readingscoll = new Fongo("fakemongo").getDB("meterdb").getCollection("readings")
      readingscoll.count() must_== 0
      readingscoll.insert(MongoDBObject("_id" -> s"""${"123".trim()}-${DateTime.now().toString()}-generation""") ++
        ("type" -> "generation") ++
        ("meterId" -> "123") ++
        ("timestamp" -> DateTime.now()) ++
        ("value" -> 1.8))
      readingscoll.count() must_== 1

      ok
    }
  }
  
  "writing via FongoStorage" should {

    "overwrite" in {

      val dt = DateTime.parse("2015-03-29T00:30:00.000+01:00").toDateTime(DateTimeZone.UTC)
      val ld = dt.toLocalDate()

      FongoStorage.meterdb.getCollection("readings").remove(new MongoDBObject())
      FongoStorage.allknownMeterids() must_== List()

      val tupleWithId2 = TimeSeries("gen", "id2", ld, List(dt), List(1.8))
      FongoStorage.storeTimeseries(tupleWithId2) must_== Some(tupleWithId2)
      FongoStorage.allknownMeterids() must_== List("id2")
      FongoStorage.storeTimeseries(tupleWithId2.copy(values = List(1.9) ) )

      val (q,f,o)  = (new MongoDBObject(), new MongoDBObject(), new MongoDBObject())
      FongoStorage.findOneReading(q,f,o).get.toString() must_==
        """{ "_id" : "id2-2015-03-28T23:30:00.000Z-gen" , "type" : "gen" , "meterId" : "id2" , "timestamp" : { "$date" : "2015-03-28T23:30:00.000Z"} , "value" : 1.9 , "unit" :  null }"""
      
      FongoStorage.asTimeseries(FongoStorage.findOneReading(q,f,o).get ).toString must_== tupleWithId2.copy(values = List(1.9) ).toString 
      
      import scala.collection.JavaConversions._
      
      FongoStorage.meterdb.getCollection("totals").find().toArray().toList foreach println

      ok
    }

  }

}