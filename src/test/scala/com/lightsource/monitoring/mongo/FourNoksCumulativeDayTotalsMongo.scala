package com.lightsource.monitoring.mongo

import java.io.File
import java.util.Date
import scala.io.Source
import scala.runtime.ZippedTraversable3.zippedTraversable3ToTraversable
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.common.TimeSeries
import com.mongodb.BasicDBObject
import com.mongodb.casbah.commons.MongoDBObject
import org.specs2.runner.JUnitRunner
import org.joda.time.LocalDate

@RunWith(classOf[JUnitRunner])
class FourNoksCumulativeDaytotalsMongo extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

  "importing asl csv file into mongodb" should {

    "work" in {
      
      skipped("move to Fongo or remove")

      import com.mongodb.casbah.Imports._
      
      import com.mongodb.casbah.commons.MongoDBObject

      val m = MongoDBObject()
      
      println(MongoStorage.findOneReading(("timestamp" $lte DateTime.now().minusMonths(1) ) ++ ("meterId" $eq "5EA94940".r),  m, ("timestamp" $eq -1)) )
      println(MongoStorage.findOneReading(("meterId" $regex "5EA94940"),  m, MongoDBObject("timestamp" -> -1)) )
      println(MongoStorage.findOneReading(("meterId" $eq "5EA94940".r),  m, ("timestamp" $eq -1)) )
      
      println("===")

      def days(dt: DateTime): Stream[DateTime] = dt #:: days(dt.minusDays(1))
      
      val daysstartofyear = days(DateTime.now()) take (DateTime.now().getDayOfYear)
      val lastondaydbos = daysstartofyear.flatMap { dt => MongoStorage.findOneReading(("timestamp" $lte dt ) ++ ("meterId" $eq "5EA94940".r),  m, ("timestamp" $eq -1)) }
      
//      lastondaydbos.map { dbo => dbo.get("timestamp").getClass } foreach println
      
      
      val lastondayts = lastondaydbos map MongoStorage.asTimeseries
      lastondayts foreach MongoStorage.storeDayTotal2
      
      
      ok
    }
  }
}