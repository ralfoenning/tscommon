package com.lightsource.monitoring.mongo

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.json4s._
import org.json4s.native.JsonParser
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.common.{TimeSeries, MongoStorage}
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import org.joda.time.DateTimeZone

@RunWith(classOf[JUnitRunner])
class MongoMeterReadTest extends Specification {

  sequential

  "readings" should {

    "be displayable in Highchart" in {

      val ld = LocalDate.now()
      val dt = DateTime.parse("2015-03-29T00:30:00.000+01:00").toDateTime(DateTimeZone.UTC)

      FongoStorage.meterdb.getCollection("readings").remove(new MongoDBObject())
      FongoStorage.meterdb.getCollection("totals").remove(new MongoDBObject())

      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt), List(1.8)))
      FongoStorage.storeTimeseries(TimeSeries("gen", "id2", ld, List(dt.minusDays(1)), List(1.5)))

      import scala.collection.JavaConversions._
      println(FongoStorage.meterdb.getCollection("readings").find().sort(MongoDBObject("timestamp" -> 1)).iterator().map { x => x.toString }.mkString(","))

      val dboit = FongoStorage.meterdb.getCollection("readings").find().sort(MongoDBObject("timestamp" -> 1)).iterator()

      val jvalit = dboit.map { dbo => JsonParser.parse(dbo.toString) }

      val jvalits = jvalit.toSeq

      jvalits.map { jv => jv \\ "timestamp" } foreach println

      val jvalit2 = jvalits.map { jv: JValue =>
        {
          val newjv = jv.transformField {
            case ("timestamp", _) => ("timestamp", jv \\ "$date")
          }
          compact(render(newjv))
        }
      }

      jvalit2.mkString(",") must_== """{"_id":"id2-2015-03-27T23:30:00.000Z-gen","type":"gen","meterId":"id2","timestamp":"2015-03-27T23:30:00.000Z","value":1.5,"unit":null},{"_id":"id2-2015-03-28T23:30:00.000Z-gen","type":"gen","meterId":"id2","timestamp":"2015-03-28T23:30:00.000Z","value":1.8,"unit":null}"""

      ok
    }
  }

}