package com.lightsource.monitoring.mongo

import java.io.File
import scala.runtime.ZippedTraversable3.zippedTraversable3ToTraversable
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.common.TimeSeries
import com.mongodb.casbah.commons.MongoDBObject
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import scala.io.Source

@RunWith(classOf[JUnitRunner])
class FourNoksHistoricalCumulativeDaytotalsTest extends Specification {

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())
  
  sequential

  "totals for cumulative timeseries" should {

    "work on 15 min interval file" in {

      FongoStorage.meterdb.getCollection("readings").drop()
      FongoStorage.meterdb.getCollection("totals").drop()

      val f = fileResource("fournokstoddhist15min.csv")

      assertUnits(f)

      val listOfTimeseries = parseIntoTimeseries(f)

      listOfTimeseries foreach println

      listOfTimeseries foreach FongoStorage.storeTimeseries

      FongoStorage.countReadings(MongoDBObject()) must_== Source.fromFile(f).getLines().toList.tail.size * (15 + 8)

      import com.mongodb.casbah.Imports._

      import scala.collection.JavaConversions._
      FongoStorage.meterdb.getCollection("readings").find(("timestamp" $lte DateTime.now()) ++ ("meterId" $eq "5EA94940-generener")).toArray.toList foreach println

      storeTotals("5EA94940-generener")
      FongoStorage.meterdb.getCollection("totals").find().toArray().toList foreach println

      //should have one total for each distinct date in src file
      FongoStorage.meterdb.getCollection("totals").count() must_== listOfTimeseries.map(_.date).toSet.size
      FongoStorage.meterdb.getCollection("totals").find().toArray.toList.map(dbo => new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC).toLocalTime()) foreach println
      
      
      FongoStorage.meterdb.getCollection("totals").find().toArray.toList.map(dbo => new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC).toLocalTime()).foreach(
          t => t.toString must_== "00:00:00.000")

      ok
    }
    
    "work on 12h interval file" in  {
      
      FongoStorage.meterdb.getCollection("readings").drop()
      FongoStorage.meterdb.getCollection("totals").drop()

      val f = fileResource("fournokstoddhist12h.csv")

      assertUnits(f)

      val listOfTimeseries = parseIntoTimeseries(f)

      listOfTimeseries foreach println

      listOfTimeseries foreach FongoStorage.storeTimeseries

      FongoStorage.countReadings(MongoDBObject()) must_== Source.fromFile(f).getLines().toList.tail.size * (15 + 8)

      import com.mongodb.casbah.Imports._

      import scala.collection.JavaConversions._
      FongoStorage.meterdb.getCollection("readings").find(("timestamp" $lte DateTime.now()) ++ ("meterId" $eq "5EA94940-generener")).toArray.toList foreach println

      storeTotals("5EA94940-generener")
      FongoStorage.meterdb.getCollection("totals").find().toArray().toList foreach println

      //should have one total for each distinct date in src file
      FongoStorage.meterdb.getCollection("totals").count() must_== listOfTimeseries.map(_.date).toSet.size
      FongoStorage.meterdb.getCollection("totals").find().toArray.toList.map(dbo => new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC).toLocalTime()) foreach println
      
      
      FongoStorage.meterdb.getCollection("totals").find().toArray.toList.map(dbo => new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC).toLocalTime()).foreach(
          t => t.toString must_== "00:00:00.000")

      ok
    
      
      ok
      
    }
  }

  def parseIntoTimeseries(f: File) = {

    val bufferedSrc = Source.fromFile(f)
    val srcLines = bufferedSrc.getLines().toList

    val listOfSplittedLines = srcLines map (l => l.split(";"))

    println(listOfSplittedLines.head.toList)

    val olabels = listOfSplittedLines.head.toList

    val dblabels = List("generpwr", "generener", "genenerdelta", "soldpwr", "soldener", "bougpwr", "bougenertot", "bougenerf1", "bougenerf2", "conspwr", "consener", "consenerdelta", "extalarm1", "extalarm2", "eliosflag", "bougenerf3")

    val regex = """\[(.*?)\]"""r

    val units = olabels.tail.map { x => regex.findFirstMatchIn(x).map(rm => rm.group(1)) }

    println(units)

    olabels.tail.zip(dblabels) map (t => println(t._1 + " " + t._2))

    val makeDouble = (s: String) => {
      try {
        s.replace(',', '.').toDouble
      } catch {
        case nfe: NumberFormatException => -1
      }
    }

    val res5 = listOfSplittedLines.tail.map(t => {

      val firstcol = t.head
      println(firstcol)
      val regex3 = """"=""(.*?)"""""r

      val dtstring = regex3.findFirstMatchIn(firstcol).map { rm => rm.group(1) }.get

      val ldt = DateTimeFormat.forPattern("dd-MM-yy HH:mm:ss").parseLocalDateTime(dtstring)
      val dtZ = ldt.toDateTime(DateTimeZone.UTC)

      val data = t.tail

      val regex2 = """"(.*?)""""r

      val unquoteddata = data.toList.flatMap { quotedstr => regex2.findFirstMatchIn(quotedstr).map { rm => rm.group(1) } }

      println(data.toList)

      (unquoteddata, dblabels, units).zipped.toList.map { v =>
        TimeSeries(v._2,
          "5EA94940" + "-" + v._2,
          ldt.toLocalDate(),
          List(dtZ),
          List(makeDouble(v._1)),
          None,
          v._3,
          List(makeDouble(v._1)),
          false)
      }

    })

    val listOfTimeseries = res5.flatten

    listOfTimeseries

  }

  def assertUnits(f: File) = {

    val bufferedSrc = Source.fromFile(f)
    val srcLines = bufferedSrc.getLines().toList

    val listOfSplittedLines = srcLines map (l => l.split(";"))

    println(listOfSplittedLines.head.toList)

    val olabels = listOfSplittedLines.head.toList

    val dblabels = List("generpwr", "generener", "genenerdelta", "soldpwr", "soldener", "bougpwr", "bougenertot", "bougenerf1", "bougenerf2", "conspwr", "consener", "consenerdelta", "extalarm1", "extalarm2", "eliosflag", "bougenerf3")

    val regex = """\[(.*?)\]"""r

    val units = olabels.tail.map { x => regex.findFirstMatchIn(x).map(rm => rm.group(1)) }

    units.size must_== 15 //15 timeseries
    units.filter(_.get.toLowerCase() == "kwh").size must_== 8 //8 of which will cause a money one to be created

  }

  def storeTotals(mid: String) = {

    import com.mongodb.casbah.Imports._

    def days(dt: LocalDateTime): Stream[LocalDateTime] = dt #:: days(dt.plusDays(1))

    val daysstartofyear = days(new LocalDateTime(2015, 1, 1, 0, 0, 0)) take (LocalDate.now().getDayOfYear)

    import com.mongodb.casbah.commons.MongoDBObject
    val lastondaydbos = daysstartofyear.flatMap { dt =>
      FongoStorage.findOneReading(("timestamp" $lt dt.toDateTime(DateTimeZone.UTC)) ++ ("meterId" $eq mid), MongoDBObject(), ("timestamp" $eq -1))
    }
    val lastondayts = lastondaydbos map FongoStorage.asTimeseries

    lastondaydbos foreach println
    lastondayts foreach println

    lastondayts foreach FongoStorage.storeDayTotal2

  }
}