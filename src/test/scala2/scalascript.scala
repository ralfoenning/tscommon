object HelloWorld {
  def main(args: Array[String]) {
    println("Hello, world! " + args.toList)
  }
}
import java.io.File
import com.lightsource.monitoring.common.TimeSeries

  def fileResource(resourcePath: String) = new File(this.getClass().getResource("/" + resourcePath).toURI())

      TimeSeries("v._2",
        "5EA94940" + "-" + "v._2",
        null,
        List(),
        List(),
        None,
        Some("v._3"),
        List(),
        false)

HelloWorld.main(args)