package com.lightsource.monitoring.alerting

import java.util.Date
import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import com.lightsource.monitoring.common.MongoStorage
import com.lightsource.monitoring.common.MonitoringStorage
import com.mongodb.casbah.Imports._

class NoDataDetector {

  println("hello from NoDataDetector")

  val timestampOf = (e: DBObject) => new LocalDateTime(e.get("timestamp"))
  val meteridOf = (e: DBObject) => e.get("meterId").asInstanceOf[String]
  val timesToCheckFor = (e: DBObject) => {
    def everyTwentyfourHoursStartingAt(x: DateTime): Stream[DateTime] = x #:: everyTwentyfourHoursStartingAt(x.plusDays(1))
    val ts = timestampOf(e).plusDays(1)
    val start = new LocalDateTime(ts.getYear, ts.getMonthOfYear, ts.getDayOfMonth, 0, 0, 0).plusHours(16).toDateTime()
    everyTwentyfourHoursStartingAt(start) takeWhile { _.isBeforeNow() } toList
  }
  val isPrecededByLargeGap = (meterid: String, dt: DateTime, rep: MonitoringStorage) => {
    val res = rep.numberOfEntriesForInInterval(meterid, dt.minusHours(12), dt)
    println("xxxxxxxxx-" + meterid + "-" + dt + "-" + res)
    res == 0
  }

  val oldestEntries = (rep: MonitoringStorage) => rep.allknownMeterids().flatMap { each => rep.oldestEntryFor(each) }
  val createAllAlarmsOn = (rep: MonitoringStorage) =>
    for (
      e <- oldestEntries(rep);
      dt <- timesToCheckFor(e)
    ) {
      println("''''''-" + meteridOf(e) + "-" + dt + "-" +  isPrecededByLargeGap(meteridOf(e), dt, rep) )
      val fillVal = if (isPrecededByLargeGap(meteridOf(e), dt, rep)) 0.0 else 1.0
        Alerting(rep).produceAlarmActions(None)(
          e.get("type").asInstanceOf[String],
          meteridOf(e),
          dt.toLocalDate(),
          List(),
          List.fill[Double](48)(fillVal)) map (_.doit())
      
    }
}

object NoDataDetector extends App {

  new NoDataDetector().createAllAlarmsOn(MongoStorage)

}

  
