package com.lightsource.monitoring.alerting

import com.lightsource.monitoring.common.MonitoringStorage
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import grizzled.slf4j.Logging
import org.joda.time.{ DateTime, DateTimeZone, LocalDate, LocalTime }

object GapKind extends Enumeration {
  type GapKind = Value
  val NoGap, StartGap, EndGap, MidGap, StartEndGap = Value
}
import GapKind._

object AlarmAction extends Enumeration {
  type AlarmAction = Value
  val CloseIfAny, New, Nothing = Value
}
import AlarmAction._

case class Alerting(rep: MonitoringStorage) extends Logging {

  RegisterJodaTimeConversionHelpers()

  class Actioner(action: AlarmAction)(val meterid: String, val csvday: DateTime) extends AlarmActioner {

    def _doit() = action match {

      case CloseIfAny => {
        logger.info("closing alarm: " + meterid + "-" + csvday)
        rep.closeAlarm(meterid, csvday)
      }
      case New => {
        logger.info("inserting alarm: " + meterid + "-" + csvday)
        rep.createAlarm(meterid, csvday)
      }
      case _ => {}
    }

    override def toString = action.toString()
  }

  val produceAlarmActions = _produceAlarmActions _

  def _produceAlarmActions(hasOldOngoingAlarms: Option[(String, LocalDate) => Boolean] = None)(
    meaningOfVals: String,
    meterid: String,
    date: LocalDate,
    iso8601Times: List[DateTime],
    daydvalues: List[Double]): List[AlarmActioner] = {

    val evalPeriod = daydvalues.slice(21, 48 - 21); logger.info("eval slice: " + evalPeriod)

    val gapOrNotAction = getGapKind(evalPeriod) match {

      case NoGap => CloseIfAny

      case _ if (!hasOldOngoingAlarms.getOrElse(rep.hasOldOngoingAlarm _)(meterid, date)) => { //gap and no prior alarm
        println(date)
        New
      }

      case _ => Nothing
    }

    val doesGapEndAction = getGapKind(evalPeriod) match {

      case StartGap | MidGap => CloseIfAny //gap that ends

      case _                 => Nothing
    }

    val allActions = List(gapOrNotAction, doesGapEndAction); logger.info(allActions)

    val dtForCasbah = date.toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
    allActions.map { enum => new Actioner(enum)(meterid, dtForCasbah) }
  }

  lazy val zeroSegs: Seq[(Int, Double)] => List[List[(Int, Double)]] = list => zeroSegs[Int](list)

  def zeroSegs[T](list: Seq[(T, Double)]) = {
    val res =
      list.foldLeft(List(List[(T, Double)]())) {
        case (List() :: t, (i, j)) if (j != 0) => List() :: t
        case (h :: t, (i, 0.0))                => ((i, 0.0) :: h) :: t
        case (h :: t, nonzero)                 => List() :: (h :: t)

      } reverse

    res.filterNot(_.isEmpty)
  }
  def hasNoGaps(l: Seq[Double]) = zeroSegs((1 to l.size).zip(l)).isEmpty
  def hasStartGap(l: Seq[Double]) = zeroSegs((1 to l.size).zip(l)).flatMap { s: List[(Int, Double)] => s.map(_._1) }.contains(1)
  def hasEndGap(l: Seq[Double]) = hasStartGap(l.reverse)
  def hasStartEndGap(l: Seq[Double]) = hasStartGap(l) && hasEndGap(l)
  def hasMidGapOnly(l: Seq[Double]) = !hasNoGaps(l) && !hasStartGap(l) && !hasEndGap(l)

  def getGapKind(l: Seq[Double]) = l match {

    case l if hasNoGaps(l)      => NoGap
    case l if hasStartEndGap(l) => StartEndGap
    case l if hasStartGap(l)    => StartGap
    case l if hasEndGap(l)      => EndGap
    case l if hasMidGapOnly(l)  => MidGap
  }

}