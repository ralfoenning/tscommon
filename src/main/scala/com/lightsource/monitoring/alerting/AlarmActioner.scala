package com.lightsource.monitoring.alerting

import grizzled.slf4j.Logging

trait AlarmActioner extends Logging {

  def _doit()

  def doit() =
    try {
      _doit()
    } catch { case e: Exception => logger.info(e.toString()) }
}

