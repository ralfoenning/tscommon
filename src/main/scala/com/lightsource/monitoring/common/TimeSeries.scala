package com.lightsource.monitoring.common

import java.text.ParseException
import java.util.Date

import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{DateTime, DateTimeZone, LocalDate}
import org.json4s._
import org.json4s.native.JsonMethods._

object SpecialFormats extends DefaultFormats {
  override val dateFormat: DateFormat = new DateFormat {
    def parse(s: String) = try {
      Some(formatter.parseDateTime(s).toDate())
    } catch {
      case e: ParseException => None
    }

    def format(d: Date) = formatter.print(new DateTime(d).toDateTime(DateTimeZone.UTC))

    private[this] def formatter = ISODateTimeFormat.dateTime() //new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
  }
}
case class TimeSeries(
    val meaning: String, val mid: String, val date: LocalDate, val times: List[DateTime], val values: List[Double], 
    showing: Option[String]=None, unit: Option[String]=None, val cumulativevalues: List[Double] = Nil, val cumulative: Boolean = false) {
  val tupled = (meaning, mid, date, times, values, showing, unit)

  implicit val formats = SpecialFormats // Brings in default date formats etc.
  lazy val toJson = compact(render(Extraction.decompose(TimeSeriesD(meaning, mid, date.toDate(), times.map(_.toDate()), values, showing))))
}

case class TimeSeriesD(val meaning: String, val mid: String, val date: Date, val times: List[Date], val values: List[Double], 
    showing: Option[String] = None, unit: Option[String]=None) {
  val jodatupled = (meaning, mid, new LocalDate(date), times.map(new DateTime(_)), values, showing)
}

object TimeSeries {
  implicit val formats = SpecialFormats // Brings in default date formats etc.

  val fromJson: String => TimeSeries = json => {
    val tsd = org.json4s.native.JsonParser.parse(json).extract[TimeSeriesD]
    TimeSeries(tsd)

  }
  def apply(tsd: TimeSeriesD) = new TimeSeries(tsd.meaning, tsd.mid, new LocalDate(tsd.date), tsd.times.map(new DateTime(_).toDateTime(DateTimeZone.UTC)), tsd.values, tsd.showing)
}
