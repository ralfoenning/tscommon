package com.lightsource.monitoring.common

import com.mongodb.DBObject
import org.joda.time.{ DateTime, LocalDate }
import com.mongodb.casbah.Imports

trait MonitoringStorage {

  def allknownMeterids(): List[String]
  def mostrecentEntryFor(meterid: String): Option[DBObject]
  def oldestEntryFor(meterid: String): Option[DBObject]
  def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime): Long

  def hasOldOngoingAlarm(meterid: String, date: LocalDate): Boolean
  def findInstallationNameForMeter(meterid: String): Option[Object]

  def createAlarm(meterid: String, from: DateTime): Unit
  def closeAlarm(meterid: String, to: DateTime): Unit

  val storeTimeseries: TimeSeries => Option[TimeSeries]
  val storeShowing: TimeSeries => Option[Int]

  def findOneReading(query: Imports.DBObject, fields: Imports.DBObject, orderBy: Imports.DBObject): Option[DBObject]
  val asTimeseries: DBObject => TimeSeries 

  //============derived timeseries==============  

  def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject]
  def findPostProcessingTimeseries(meterid: String): String
  def getRateForTimeAndMeterId(dt: DateTime, meterid: String): Option[Double]

}