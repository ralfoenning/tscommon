package com.lightsource.monitoring.common

import scala.collection.JavaConversions.asScalaBuffer
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import com.mongodb.DBObject
import com.mongodb.DuplicateKeyException
import com.mongodb.casbah.Imports.$set
import com.mongodb.casbah.Imports.JodaDateTimeDoNOk
import com.mongodb.casbah.Imports.mongoNestedDBObjectQueryStatements
import com.mongodb.casbah.Imports.mongoQueryStatements
import com.mongodb.casbah.Imports.wrapDBObj
import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.commons.Imports
import com.mongodb.casbah.commons.MongoDBList
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import grizzled.slf4j.Logging
import org.joda.time.LocalDateTime
import com.mongodb.BasicDBObject
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import java.util.Date

class MongoStorage(val meterdb: hasGetCollectionMethod) extends Logging with MonitoringStorage {

  def allknownMeterids() = meterdb.getCollection("readings").distinct("meterId").toArray().toList.asInstanceOf[List[String]]

  def findOneReading(query: Imports.DBObject, fields: Imports.DBObject, orderBy: Imports.DBObject): Option[DBObject] = {
    val res = meterdb.getCollection("readings").findOne(query, fields, orderBy)
    if (res == null) None else Some(res)
  }
  def countReadings(template: DBObject) = meterdb.getCollection("readings").count(template).toLong

  final def mostrecentEntryFor(meterid: String) = {
    val query = MongoDBObject("meterId" -> meterid)
    val fields = MongoDBObject( /*"timestamp" -> 1*/ )
    val orderBy = MongoDBObject("timestamp" -> -1) //descending
    findOneReading(query, fields, orderBy)
  }

  final def oldestEntryFor(meterid: String) = {
    val query = MongoDBObject("meterId" -> meterid)
    val fields = MongoDBObject( /*"timestamp" -> 1*/ )
    val orderBy = MongoDBObject("timestamp" -> 1) //ascending
    findOneReading(query, fields, orderBy)
  }

  final def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime) = {

    import com.mongodb.casbah.Imports._
    countReadings(("timestamp" $lte to $gte from) ++ ("meterId" -> meterid))
  }

  private val alarmcoll = meterdb.getCollection("alarms")

  def hasOldOngoingAlarm(meterid: String, startingOnOrBefore: LocalDate) = {
    import com.mongodb.casbah.Imports._
    RegisterJodaTimeConversionHelpers()

    val res = alarmcoll.find("meterId" $eq meterid).toArray().toList
    res foreach println
    val dtForCasbah = startingOnOrBefore.toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
    alarmcoll.count(("from" $lte dtForCasbah) ++ ("meterId" -> meterid) ++ ("to" -> null)) > 0
  }

  def findInstallationNameForMeter(meterid: String) = {

    val lscrmdb = MongoClient("localhost", 27017)("lscrmdb")
    val installationscoll = lscrmdb("customerInstallations")
    val installationWhereMetersListContainsMeterid = installationscoll.find(MongoDBObject()).
      find { each =>
        {
          each.getAs[MongoDBList]("installationMeters").map { eachList => eachList.contains(meterid) }.getOrElse(false)
        }
      }
    installationWhereMetersListContainsMeterid.map { eachdbo => eachdbo.get("installationDescription") }
  }

  protected val makeAlarmObject = (meterid: String, from: DateTime, installationName: Option[Object]) => MongoDBObject("_id" -> (meterid + "-" + from)) ++
    ("installation" -> installationName) ++
    ("description" -> "No Production") ++
    ("meterId" -> meterid) ++
    ("from" -> from) ++
    ("to" -> null)

  def createAlarm(meterid: String, from: DateTime) = {
    val installationName = findInstallationNameForMeter(meterid)
    println("inserting " + meterid + "-" + from + " - " + installationName)
    val result = alarmcoll.insert(makeAlarmObject(meterid, from, installationName))

    //<<<<>>>>result.toString()
  }
  def closeAlarm(meterid: String, to: DateTime) = {
    println("closing " + meterid + "-" + to)
    val result = alarmcoll.update(MongoDBObject("meterId" -> meterid) ++ ("to" -> null), $set("to" -> to), false, true)

    //<<<<>>>>result.toString()
  }

  RegisterJodaTimeConversionHelpers()

  /*protected[mongo]*/ val produceObjectTimeSeries = (ts: TimeSeries) => {

    val (whatTheValuesMean, meterId, date, iso8601Times, values, showing, unit) = ts.tupled

    val whatTheValuesMean2 = whatTheValuesMean + (if (ts.cumulative) "-cumulative" else "")
      
    val createMongoOjbFrom = (time: DateTime, meterValue: Double) => {
      MongoDBObject("type" -> whatTheValuesMean2) ++
        ("meterId" -> meterId) ++
        ("timestamp" -> time) ++
        ("value" -> meterValue) ++
        ("unit" -> unit) //or         ("unit" -> s.unit.getOrElse("n/a")) as in cloudclient,     or not all 
    }

    val output = iso8601Times.zip(values).map(createMongoOjbFrom.tupled(_))

    output
  }
  
  val asTimeseries = (dbo: DBObject) => {
    
    val dt = dbo.get("timestamp")
    
    TimeSeries(dbo.get("type").toString, 
        dbo.get("meterId").toString,
        new LocalDate(dt),
        List(new DateTime(dt).toDateTime(DateTimeZone.UTC)),
        List(dbo.get("value").toString.toDouble),
        None, 
        dbo.getAs[String]("unit").getOrElse("") match { case "" | "n/a" => None; case u @ _ => Some(u) })
  }

  val storeTimeseries: TimeSeries => Option[TimeSeries] = ts => {

    val storeResults = produceObjectTimeSeries(ts).map { dbo => storeMeterValue(dbo, "readings") }
    logger.info(storeResults)

    storeShowing2(ts)

    val wasNew = storeResults.exists { (200 to 299).contains(_) }

    if (wasNew && ts.times.size!=1) storeDayTotals(ts)

    getMoney(ts) map storeTimeseries

    if (wasNew) Some(ts) else None

  }

  def getMoney(ts: TimeSeries) = {
    ts.copy(unit = ts.unit.map(_.toLowerCase)) match {
      case TimeSeries(mean @ _, mid @ _, day, times @ _, values @ _, _, Some("kwh"), _, _) => {
        val rate = getRateForTimeAndMeterId(times(0), mid).getOrElse(Double.NaN)
        val newmean = mean + "-pence"
        val newmid = mid + "-" + newmean
        Some(TimeSeries(newmean, newmid, day, times, values.map(_ * rate), None, Some("pence")))
      }
      case _ => { logger.warn(s"no tariff found for ${ts.mid} at ${ts.times(0)}"); None }
    }
  }

  def storeShowing2(ts: TimeSeries) = {

    ts match {
      case TimeSeries(mean @ _, mid @ _, _, times @ _, _, Some(""), _, _, _) => {

        val lastShowing = mostrecentEntryFor(mid + "-showing").getOrElse(MongoDBObject("value" -> Int.MinValue)).get("value").toString().toDouble
        val showingVal = ts.values.foldLeft(0.0)(_ + _).toInt + lastShowing.toInt

        val obj = MongoDBObject("type" -> (mean + "-showing")) ++
          ("meterId" -> (mid + "-showing")) ++
          ("timestamp" -> times(0)) ++
          ("value" -> showingVal)

        Some(storeMeterValue(obj, "readings"))
      }
      case TimeSeries(mean @ _, mid @ _, _, times @ _, _, Some(showingVal), _, _, _) => {
        val obj = MongoDBObject("type" -> (mean + "-showing")) ++
          ("meterId" -> (mid + "-showing")) ++
          ("timestamp" -> times(0)) ++
          ("value" -> showingVal)

        Some(storeMeterValue(obj, "readings"))
      }
      case _ => None
    }

  }

  def storeDayTotals(ts: TimeSeries) {

    val dayPartitions = ts.times.grouped(48).toList.zip(ts.values.grouped(48).toList)
//    dayPartitions.foreach(dtval => storeDayTotal(ts.meaning, ts.mid, ts.date, dtval._1, dtval._2))

    dayPartitions.foreach(dtval => storeDayTotal2(ts.copy(times = dtval._1, values = dtval._2)) )
  }

//  val storeDayTotal: (String, String, LocalDate, List[DateTime], List[Double]) => 
//    Option[(String, String, LocalDate, List[DateTime], List[Double])] = (t, mid, c, d, v) => {
//
//    val sum = v.foldLeft(0.0)(_ + _)
//
//    val daytotal =
//      MongoDBObject("type" -> ("dayTotal-" + t)) ++
//        ("meterId" -> mid) ++
//        ("timestamp" -> d(0)) ++
//        ("value" -> sum)
//
//    storeMeterValue(daytotal, "totals")
//
//    None
//  }
  
  val storeDayTotal2: TimeSeries => Option[TimeSeries] = ts => {
    
    val (t, mid, c, d, v, s, u) = ts.tupled

    val sum = v.foldLeft(0.0)(_ + _)

    val daytotal =
      MongoDBObject("type" -> ("dayTotal-" + t)) ++
        ("meterId" -> mid) ++
        ("timestamp" -> new LocalDateTime(d(0).getYear, d(0).getMonthOfYear, d(0).getDayOfMonth, 0, 0, 0).toDateTime(DateTimeZone.UTC)) ++
        ("value" -> sum)

    storeMeterValue(daytotal, "totals")

    None
  }

  def storeMeterValue(dbo: DBObject, collection: String) = {

    val meterpart = dbo.getAs[String]("meterId").get.trim()
    val timestamppart = new DateTime(dbo.get("timestamp")).toDateTime(DateTimeZone.UTC)
    val typepart = dbo.getAs[String]("type").get
    val id = MongoDBObject("_id" -> s"""${meterpart}-${timestamppart}-${typepart}""")

    writeAsCreateOnly(dbo, id, collection)
  }

  def writeAsCreateOnly(dbo: DBObject, id: DBObject, collection: String) = {
    logger.info(dbo)
    try {
      val result = meterdb.getCollection(collection).update(id, dbo, true, false)
//      logger.info("isUpdateOfExisting: " + result.isUpdateOfExisting())
      logger.info("tscommon 23715")
      201
    } catch { case e: DuplicateKeyException => /*e.printStackTrace();*/ 409 }
  }

  val storeShowing: TimeSeries => Option[Int] = ts => {

    ts match {
      case TimeSeries(mean @ _, mid @ _, _, times @ _, _, Some(showingVal), _, _, _) => {
        val obj = MongoDBObject("type" -> (mean + "-showing")) ++
          ("meterId" -> (mid + "-showing")) ++
          ("timestamp" -> times(0)) ++
          ("value" -> showingVal)

        Some(storeMeterValue(obj, "readings"))
      }
      case _ => None
    }
  }

  //============methods for derived timeseries==============

  def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject] = {
    import com.mongodb.casbah.Imports._
    findReadings(("timestamp" $lte to $gte from) ++ ("meterId" -> meterid))
  }

  protected def findReadings(template: DBObject): List[DBObject] = {

    println(template)

    meterdb.getCollection("readings").find(template).limit(5000).toArray().toList
  }

  def findPostProcessingTimeseries(meterid: String) = {

    Map(
      "14172978" -> ("2394000121232", "consumption"),
      "14172974" -> ("2394000121241", "consumption"))

    Map(
      "14172978" -> "2394000121232",
      "14172974" -> "2394000121241",
      "2394000121241" -> "14172974",
      "2394000121232" -> "14172978").get(meterid).get
  }

  def getRateForTimeAndMeterId(dt: DateTime, meterid: String) = {

    val day = new LocalDateTime(dt.getYear, dt.getMonthOfYear, dt.getDayOfMonth, 0, 0, 0).toDateTime(DateTimeZone.UTC)
    val curs = meterdb.getCollection("tariffs").find(MongoDBObject("meterId" -> meterid) ++ ("timestamp" -> day))
    if (curs.hasNext()) Some(curs.next().get("rate").asInstanceOf[String].toDouble) else None
  }

}

object MongoStorage extends MonitoringStorage {

  val theStorage = new MongoStorage(MongoClient("localhost", 27017)("meterdb"))

  override def allknownMeterids() = theStorage.allknownMeterids()
  override def mostrecentEntryFor(meterid: String) = theStorage.mostrecentEntryFor(meterid)
  override def oldestEntryFor(meterid: String) = theStorage.oldestEntryFor(meterid)
  override def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime): Long = theStorage.numberOfEntriesForInInterval(meterid, from, to)

  override def hasOldOngoingAlarm(meterid: String, date: LocalDate) = theStorage.hasOldOngoingAlarm(meterid, date)

  override def createAlarm(meterid: String, from: DateTime) = theStorage.createAlarm(meterid, from)

  override def findInstallationNameForMeter(meterid: String) = theStorage.findInstallationNameForMeter(meterid)

  override def closeAlarm(meterid: String, to: DateTime) = theStorage.closeAlarm(meterid, to)

  override val storeTimeseries = theStorage.storeTimeseries

  override val storeShowing: TimeSeries => Option[Int] = theStorage.storeShowing

  override def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject] = theStorage.entriesForInInterval(meterid, from, to)

  override def findPostProcessingTimeseries(meterid: String): String = theStorage.findPostProcessingTimeseries(meterid)

  def getRateForTimeAndMeterId(dt: DateTime, meterid: String): Option[Double] = theStorage.getRateForTimeAndMeterId(dt, meterid)

  val asTimeseries: DBObject => TimeSeries = theStorage.asTimeseries

  def findOneReading(q: Imports.DBObject, f: Imports.DBObject, o: Imports.DBObject): Option[DBObject] = theStorage.findOneReading(q,f,o)
  
  val storeDayTotal2: TimeSeries => Option[TimeSeries] = theStorage.storeDayTotal2

}