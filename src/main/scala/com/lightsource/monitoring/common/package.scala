package com.lightsource.monitoring

import com.mongodb.DBCollection
package object common {

  type hasGetCollectionMethod = {
    def getCollection(name: String): DBCollection
  }
  
}