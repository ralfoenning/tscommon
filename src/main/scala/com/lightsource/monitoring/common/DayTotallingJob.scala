package com.lightsource.monitoring.common

import com.mongodb.casbah.MongoClient
import com.mongodb.casbah.commons.MongoDBObject

object DayTotallingJob {

  val storage = new MongoStorage(MongoClient("localhost", 27017)("meterdb"))

  val (q, f, o) = (new MongoDBObject(), new MongoDBObject(), new MongoDBObject())

  import com.mongodb.casbah.Imports._

  storage.findOneReading(q, f, o)

}